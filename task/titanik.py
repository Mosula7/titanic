import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv(r'train.csv')
    return df


def get_group(name):
    for group in ["Mr.", "Mrs.", "Miss."]:
        if group in name:
            return group


def get_filled():
    df = get_titatic_dataframe()
    groups = ["Mr.", "Mrs.", "Miss."]

    df['Group'] = df['Name'].apply(get_group)

    n_miss = []
    medians = []

    for group in groups:
        n_miss.append(df.query('Group == @group')['Age'].isna().sum())
        medians.append(df.query('Group == @group')['Age'].median())
    
    res=[]
    for i in range(len(groups)):
        res.append((groups[i],n_miss[i], medians[i]))

    return res
    

        




